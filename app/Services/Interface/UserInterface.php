<?php 

namespace App\Services\Interface;


// Nah karena sudah membuat class dan kita ingin ada fitur dependency injection nya
// kita tinggal membuat user service provider, "php artisan make:provider UserServiceProvider"
// dan file nya kan terbikin di folder providers
interface UserInterface
{
    public function getUser($id);
    public function getAllUsers();
    public function createUser($request);
    public function updateUser($request, $id);
    public function deleteUser($id);
}

?>