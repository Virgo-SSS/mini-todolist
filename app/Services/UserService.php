<?php 

namespace App\Services;

use App\Services\Interface\UserInterface;


// User service Implenet ke user Interface di folder interface
// otomatis semua function yang ada di interface wajib di implement di oleh user service

// User Service di gunakan untuk melakukan semua logic yang berhubungan dengan User, jadi kita tidak  full
// mengunakan controlller sebagai Logic nya, karena biasanya controller nya hanya di gunakan untuk logic web nya
class UserService implements UserInterface 
{

    public function getUser($id)
    {
        //
    }
    public function getAllUsers()
    {
        // 
    }
    public function createUser($request)
    {

    }
    public function updateUser($request, $id)
    {
        // 
    }
    public function deleteUser($id)
    {
        // 
    }
}

?>