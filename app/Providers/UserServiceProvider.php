<?php

namespace App\Providers;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

// kita menggunakan implements DefferableProvider karena kita ingin menjadikan nya sebagai lazy 
class UserServiceProvider extends ServiceProvider implements DeferrableProvider
{
    // nah jangan lupa karena kita mengimplements DeferrableProvider, kita harus membuat method atau mengoverride
    // method provides (silahkan cek di class deferrable provider (ctrl + click untuk melihat))
    
    public array $singleton = [
        // nah karena kita sederhana saja jdi kita membuat singleton, jadi ketika ada yg membutuhkan user interface,
        // kita tingal menginjeksi nya ke user service
        UserInterface::class => UserService::class, 
    ];
    public function provides (): array
    {
        return [
            UserInterface::class // ini mengarah kepada user interface di folder service/interface 
            // agar yg di return kan adalah data dari interface nya
        ];

        // kemudian setelah selesai membuat provider, provider nya kita daftarkan di config/app -> bagian providers
    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
